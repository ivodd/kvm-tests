#! /usr/bin/perl

use YAML::Tiny;
use Data::Dumper;
use strict;

sub get_items {
	my $arg = shift;
	unless ($arg) {
		return ();
	}
	if (ref($arg) eq "ARRAY") {
		return @$arg;
	} elsif (ref($arg) eq "HASH") {
		return map($arg->{$_}, sort keys %$arg);
	} else {
		return ( $arg );

	}
}

sub get_nic {
	my $nicinfo = shift;
	my $id = shift;
	my $opts = "";

	#$opts .= "-net nic,id=$id";
	$opts .= "-device e1000,netdev=$id";
	if ($nicinfo->{"mac"}) {
		#$opts .= ",macaddr=".$nicinfo->{"mac"};
		$opts .= ",mac=".$nicinfo->{"mac"};
	}
	$opts .= " ";

	if ($nicinfo->{"tap"}) {
		$opts .= "-netdev tap,id=$id,ifname=".$nicinfo->{"tap"}.",script=no,downscript=no ";
	} elsif ($nicinfo->{"vde"}) {
		$opts .= "-netdev vde,id=$id,sock=".$nicinfo->{"vde"}." ";
	} else {
		$opts .= "-netdev user,id=$id";
		foreach my $fwd (get_items($nicinfo->{"sshfwd"})) {
			$opts .= ",hostfwd=tcp:127.0.0.1:".$fwd."-:22";
		}
		$opts .= " ";
	}

	return $opts;
}

my $config = shift;
my $vm = shift;
my $vmpath = ".";

my $yaml = YAML::Tiny->read($config);

my $config = $yaml->[0]->{$vm};

unless($config) {
	die("vm $vm niet gevonden\n");
}
print Dumper $config;

my $cmd = "kvm ";
$cmd .= "-name $vm ";
$cmd .= "-m ".$config->{"ram"}." ";
$cmd .= "-vga std ";
$cmd .= "-smp 2 ";
$cmd .= "-usbdevice tablet ";

my $nicindex = 0;
foreach my $item (get_items($config->{"nics"})) {
	$cmd .= get_nic($item,"nicid$nicindex");
	$nicindex++;
}

my $diskindex = 0;
foreach my $item (get_items($config->{"image"})) {
	$cmd .= "-drive file=$vmpath/$item,cache=unsafe,index=$diskindex,media=disk ";
	$diskindex++;
}

my $fsindex = 0;
foreach my $item (get_items($config->{"shared_path"})) {
	my $path = $item->{"path"};
	my $id = $item->{"id"};
	$cmd .= "-fsdev local,security_model=none,readonly,id=fsdev${fsindex},path=$path ";
	$cmd .= "-device virtio-9p-pci,id=p9dev${fsindex},fsdev=fsdev${fsindex},mount_tag=${id} ";
	$fsindex++;
}

my $charindex = 0;
foreach my $item (get_items($config->{"monitor"})) {
	my $path = $item;
	$cmd .= "-chardev socket,id=chardev${charindex},path=${path},server,nowait -mon chardev=chardev${charindex} ";
	$charindex++;
}

foreach my $item (get_items($config->{"vnc"})) {
	my $display = $item;
	if ($display =~ m/^:?([0-9]+)$/) {
		$display = ":".$1;
	} elsif ($display =~ m;^/;) {
		$display = "unix:".$display;
	}
	$cmd .= "-display vnc=${display} ";
}

foreach my $item (get_items($config->{"usbdev"})) {
	$cmd .= "-usb -usbdevice host:$item ";
}

foreach my $item (get_items($config->{"serialnumber"})) {
	$cmd .= "-smbios type=1,serial='$item' ";
}

$cmd .= "&";

print $cmd."\n";
system($cmd);
sleep(1);
