#! /bin/bash

set -efux

exec >> /var/log/ansible-firstboot 2>&1

echo `date` starting

. /debconf-video/kvm-tests/kvm-tests.cfg

if [ -n "$apt_proxy" ]
then
	echo "Acquire::http::Proxy \"$apt_proxy\";" > /etc/apt/apt.conf.d/apt-proxy.conf
fi

grep "^deb.*backports" /etc/apt/sources.list || \
	echo "deb http://deb.debian.org/debian stretch-backports main contrib non-free" >> /etc/apt/sources.list

apt-get update

apt-get -u dist-upgrade -y

apt-get install -y git eatmydata etckeeper

apt-get -y install git-daemon-sysvinit

# export local git repo via git daemon, to allow local config in test
# environment
cat >> /etc/default/git-daemon << EOF
# added by firstboot.sh
GIT_DAEMON_ENABLE=true
GIT_DAEMON_USER=gitdaemon
GIT_DAEMON_BASE_PATH=/debconf-video
GIT_DAEMON_DIRECTORY=/debconf-video

GIT_DAEMON_OPTIONS="--export-all"
EOF

service git-daemon restart || true

# Ansible >= 2.4
suite=$(lsb_release -cs)
case $suite in
	stretch)
		apt-get install -y ansible/stretch-backports
		;;
	xenial|artful)
		apt-add-repository --yes --update ppa:ansible/ansible
		apt-get install -y ansible
		;;
	*)
		apt-get install -y ansible
		;;
esac

# clone our ansible repository(s)
# create and run a script to run ansible on the local box.

git clone $playbook_repo /root/playbook-repo
(cd /root/playbook-repo; git checkout $playbook_branch)
INVENTORY=/root/playbook-repo/inventory/hosts
PLAYBOOKS=/root/playbook-repo/site.yml

if [ "${inventory_repo}" != "" ]; then
	git clone $inventory_repo /root/inventory-repo
	(cd /root/inventory-repo; git checkout $inventory_branch)
	INVENTORY=/root/inventory-repo/inventory/hosts
	if [ -e /root/inventory-repo/site.yml ]; then
		PLAYBOOKS="$PLAYBOOKS /root/inventory-repo/site.yml"
	fi
fi

echo "$vault_pw" | base64 -d > /root/.ansible-vault
chmod 600 /root/.ansible-vault

vault_pw_arg=
if [ "$vault_pw" != "" ]; then
    vault_pw_arg="--vault-password-file=/root/.ansible-vault"
fi

script=/usr/local/sbin/ansible-up
cat > $script <<EOF
#!/bin/sh

set -euf

cd /root/

(cd playbook-repo; git pull)

if [ "${inventory_repo}" != "" ]; then
	(cd inventory-repo; git pull)
fi

ansible-playbook \\
    --inventory-file=$INVENTORY \\
    ${vault_pw_arg} \\
    --connection=local \\
    --limit=\$(hostname) \\
    $PLAYBOOKS \\
    "\$@"
EOF
chmod +x $script

# HOME is not set at firstboot, and that makes ansible fail :(
export HOME=/tmp

# ansible will certainly fail, because the interface names are different, so
# restarting networking will fail
$script || true

# cleanup cloud-init stuff
rm /etc/network/interfaces.d/50-cloud-init.cfg

apt-get -y install at

apt-get -y install man

etckeeper init -d /srv || true
etckeeper commit -d /srv -m "after ansible" || true

# TODO this should probably be done by ansible
cat /root/.ssh/authorized_keys > /srv/pxe/scripts/authorized_keys

etckeeper commit -d /srv -m "after firstboot.sh" || true
etckeeper commit -m "after firstboot.sh" || true


echo `date` schedule reboot

# removing cloud-init from the cloudinit firstboot job doesn't work very well
# do it from an at job instead
cat | at "now" << EOF
apt-get remove -y --purge cloud-init
reboot
EOF


